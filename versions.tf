terraform {
  required_providers {
    civo = {
      source  = "civo/civo"
      version = "0.10.3"
    }
    random = {
      source = "hashicorp/random"
      version = "3.1.0"
    }
  }
  required_version = ">= 0.13"
}
