# civo-k3s

This repository runs a GitLab Ci pipeline to create a k3s cluster with 3 nodes on [Civo](Civo.com) with a random pet name (example: causal-pony).

## ToDo

### Civo Credentials

Please create a variable called `VAR_CIVO_TOKEN` in your GitLab project settings with your Civo token.

* [Add a variable to a project](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)

### GitLab CI

Please rename `example_gitlab-ci.yml` to `.gitlab-ci.yml`.

### Kubeconfig

Not part of this workflow. You can manually download your `kubeconfig` via the Civo GUI.
## Pipeline execution

You have to manually execute the stage `apply` in your GitLab -> CI/CD -> Pipelines menu.

Just remove the `when` section in the `apply` stage to have a fully automated pipeline:

```yaml
  when: manual
```

## Nodes

By default Civo deployes your k3s cluster with 3 nodes.

If you want more or less nodes, simply add this line to the `civo_kubernetes_cluster` resource section in `main.tf`:

```terrafrom
num_target_nodes = 4
```


## Documenation

[Terrafrom provider documentation](https://registry.terraform.io/providers/civo/civo/latest/docs)